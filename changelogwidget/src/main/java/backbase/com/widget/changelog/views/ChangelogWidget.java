package backbase.com.widget.changelog.views;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.backbase.android.Backbase;
import com.backbase.android.model.Renderable;
import com.backbase.android.plugins.storage.BBStorage;
import com.backbase.android.plugins.storage.StorageComponent;
import com.backbase.android.plugins.storage.memory.InMemoryStorage;
import com.backbase.android.rendering.Renderer;
import com.backbase.android.rendering.inner.RendererListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import backbase.com.widget.changelog.interfaces.ChangelogSource;
import backbase.com.widget.changelog.managers.ChangelogWidgetSourceFactory;
import backbase.com.widget.changelog.views.adapters.ChangelogAdapter;
import backbase.com.widget.changelog.models.ChangelogItem;
import backbase.com.widget.changelog.interfaces.ChangelogSourcesListener;
import backbase.com.widget.changelog.R;


/**
 * Created by Backbase R&D B.V on 21/06/16.
 */
public class ChangelogWidget extends View implements Renderer, ChangelogSourcesListener, AdapterView.OnItemClickListener, View.OnClickListener {

    Backbase cxpInstance;
    private ListView changelogListView;
    private List<ChangelogItem> changelogItems;
    private Renderable renderable;
    BBStorage plugin;
    private ChangelogAdapter adapter;
    private ProgressBar progressBar;
    private RelativeLayout errorLayout;
    private ImageButton refreshButton;
    ChangelogSource changelogSource;
    private StorageComponent inMemoryStorage;

    public ChangelogWidget(Context context) {
        super(context);
    }

    @Override
    public void start(final Renderable renderable, ViewGroup insertPoint) {
        cxpInstance = Backbase.getInstance();
        this.renderable = renderable;
        View inflatedView = inflate(getContext(), R.layout.changelog_list, insertPoint);
        changelogListView = (ListView) inflatedView.findViewById(R.id.changelog_list);
        progressBar = (ProgressBar) inflatedView.findViewById(R.id.progressBar);
        errorLayout = (RelativeLayout) inflatedView.findViewById(R.id.emptyLayout);
        refreshButton = (ImageButton) inflatedView.findViewById(R.id.refreshButton);

        String dataProvider = renderable.getPreference("dataProvider");
        ChangelogWidgetSourceFactory.Sources sources = ChangelogWidgetSourceFactory.Sources.LOCAL;
        if (dataProvider.equals("structured-content")) {
            sources = ChangelogWidgetSourceFactory.Sources.SERVER;
        }

        changelogSource = ChangelogWidgetSourceFactory.getChangelogSource(getContext(), sources);
        changelogSource.getChangelog(renderable.getPreference("dataSource"), this);
        progressBar.setVisibility(View.VISIBLE);
        plugin = (BBStorage) cxpInstance.getRegisteredPlugin(InMemoryStorage.class);
        inMemoryStorage = plugin.getStorageComponent();

        refreshButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        progressBar.setVisibility(View.VISIBLE);
        errorLayout.setVisibility(View.GONE);
        changelogSource.getChangelog(renderable.getPreference("dataSource"), this);
    }

    @Override
    public void onSuccess(List<ChangelogItem> items) {
        changelogItems = items;
        adapter = new ChangelogAdapter(this.getContext(), items);
        changelogListView.setAdapter(adapter);
        changelogListView.setOnItemClickListener(this);
        progressBar.setVisibility(View.GONE);
        //Populate List

    }

    @Override
    public void onError(String errorMessage) {
        progressBar.setVisibility(View.GONE);
        errorLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        String jsonSelectedItem = new Gson().toJson(changelogItems.get(position));
        try {
            JSONObject payload = new JSONObject(jsonSelectedItem);
            Backbase.getInstance().publishEvent("versionClicked", renderable.getId(), payload);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void load(Context context, Renderable renderable, ViewGroup viewGroup) {

    }

    @Override
    public void preload(Renderable renderable) {

    }

    @Override
    public void startWithPreload(Renderable renderable, ViewGroup viewGroup) {

    }

    @Override
    public void startPreloadNotifications(Renderable renderable, boolean b) {

    }

    @Override
    public void pause() {
        changelogListView.setVisibility(View.GONE);
    }

    @Override
    public void resume() {
        changelogListView.setVisibility(View.VISIBLE);
    }

    @Override
    public void destroy() {

    }

    @Override
    public void destroy(long l) {

    }

    @Override
    public void clean() {

    }

    @Override
    public void stop() {

    }

    @Override
    public int getRendererWidth() {
        return getWidth();
    }

    @Override
    public int getRendererHeight() {
        return getHeight();
    }

    @Override
    public void moveTo(int i) {

    }

    @Override
    public void scaleTo(int i, int i1) {

    }

    @Override
    public void dispatchEvent(String s, JSONObject jsonObject) {

    }

    @Override
    public void setRendererListener(RendererListener rendererListener) {

    }

    @Override
    public void enableScrolling(boolean b) {

    }

    @Override
    public Renderable getRenderableItem() {
        return renderable;
    }
}
