/**
 * @module ext-demo-overview-content-ng
 *
 * @description
 * Default extension for widget-demo-overview-content-ng
 *
 * @requires vendor-bb-angular-ng-aria
 */
import ngAriaModuleKey from 'vendor-bb-angular-ng-aria';

// uncomment below to include CSS in your extension
import '../styles/index.css';

export const dependencyKeys = [
  ngAriaModuleKey,
];

export const hooks = {};

export const helpers = {};

export const events = {};
