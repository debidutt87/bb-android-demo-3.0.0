import widget from './index';

describe('About', () => {
  it('should export the module name', () => {
    expect(widget).toEqual('widget-demo-about-ng');
  });
});
