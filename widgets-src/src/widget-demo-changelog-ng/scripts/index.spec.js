import widget from './index';

describe('Changelog', () => {
  it('should export the module name', () => {
    expect(widget).toEqual('widget-demo-changelog-ng');
  });
});
