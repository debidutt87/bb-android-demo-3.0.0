import widget from './index';

describe('Overview Content', () => {
  it('should export the module name', () => {
    expect(widget).toEqual('widget-demo-overview-content-ng');
  });
});
