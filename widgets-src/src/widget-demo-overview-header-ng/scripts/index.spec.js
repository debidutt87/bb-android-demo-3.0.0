import widget from './index';

describe('Overview Header', () => {
  it('should export the module name', () => {
    expect(widget).toEqual('widget-demo-overview-header-ng');
  });
});
