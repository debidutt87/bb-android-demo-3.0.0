import widget from './index';

describe('Version', () => {
  it('should export the module name', () => {
    expect(widget).toEqual('widget-demo-version-ng');
  });
});
