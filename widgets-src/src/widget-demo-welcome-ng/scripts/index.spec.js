import widget from './index';

describe('Welcome', () => {
  it('should export the module name', () => {
    expect(widget).toEqual('widget-demo-welcome-ng');
  });
});
